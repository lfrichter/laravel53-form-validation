@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Listagem de clientes</h3>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>CNPJ/CPF</th>
                    <th>Data Nasc.</th>
                    <th>E-mail</th>
                    <th>Telefone</th>
                    <th>Sexo</th>
                    <th>Ação</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td>{{$client->id}}</td>
                        <td>{{$client->nome}}</td>
                        <td>{{$client->documento}}</td>
                        <td>{{$client->data_nasc}}</td>
                        <td>{{$client->email}}</td>
                        <td>{{$client->telefone}}</td>
                        <td>{{!empty($client->sexo) ? $client->sexo : null}}</td>
                        <td>
                            <ul class="list-inline list-small">
                                <li><a href="{{ route('clients.edit', ['client' => $client->id]) }}" class="btn btn-link btn-link-small">Editar</a></li>
                                <li>|</li>
                                <li><form method="post" action="{{ route('clients.destroy',['client' => $client->id]) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button id="" name="" class="btn btn-link btn-link-small" type="submit">Excluir</button>
                                    </form></li>
                            </ul>



                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <br />
            <a href="{{ route('clients.create') }}" class="btn btn-primary right">Novo cliente</a>
        </div>
    </div>


@endsection