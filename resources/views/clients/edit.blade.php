@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Administração de clientes</h3>
        </div>
        <div class="row">

            <br /><br />
            @if($errors->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif

            <form class="form-horizontal" method="post" action="{{ route('clients.update',['client' => $client->id]) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="pessoa" value="{{$pessoa}}"/>

                <fieldset>
                <!-- Form Name -->
                <legend>Novo cliente - {{$pessoa == \App\Client::PESSOA_JURIDICA ? 'Pessoa Jurídica' : 'Pessoa Física'}}</legend>


                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="nome">Nome</label>
                  <div class="col-md-4">
                  <input id="nome" name="nome" type="text" value="{{old('nome',$client->nome)}}" placeholder="Nome" class="form-control input-md">

                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="documento">
                      {{$pessoa == \App\Client::PESSOA_JURIDICA ? 'CNPJ' : 'CPF'}}
                  </label>
                  <div class="col-md-4">
                  <input id="documento" name="documento" value="{{old('documento',$client->documento)}}" type="text" placeholder="{{$pessoa == \App\Client::PESSOA_JURIDICA ? 'CNPJ' : 'CPF'}}" class="form-control input-md">

                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="email">E-mail</label>
                  <div class="col-md-4">
                  <input id="email" name="email" value="{{old('email',$client->email)}}" type="text" placeholder="E-mail" class="form-control input-md">

                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="telefone">Telefone</label>
                  <div class="col-md-4">
                  <input id="telefone" name="telefone" value="{{old('telefone',$client->telefone)}}" type="text" placeholder="Telefone" class="form-control input-md">

                  </div>
                </div>

                @if($pessoa == \App\Client::PESSOA_JURIDICA)

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="telefone">Fantasia</label>
                        <div class="col-md-4">
                            <input id="fantasia" name="fantasia" value="{{old('fantasia',$client->fantasia)}}" type="text" placeholder="Nome fantasia" class="form-control input-md">

                        </div>
                    </div>

                @else
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="estado_civil">Estado civil</label>
                      <div class="col-md-4">
                        <select id="estado_civil" name="estado_civil" class="form-control">
                            <option value="0">Selecione</option>
                            @foreach(\App\Client::ESTADOS_CIVIS as $key => $value)
                              <option value="{{$key}}" {{old('estado_civil',$client->estado_civil) == $key ? 'selected="selected"' : ''}}>{{$value}}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="data_nasc">Data de nascimento</label>
                      <div class="col-md-4">
                      <input id="data_nasc" name="data_nasc" type="date" value="{{old('data_nasc',$client->data_nasc)}}" placeholder="Data de nascimento" class="form-control input-md">

                      </div>
                    </div>

                    <!-- Multiple Radios -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="sexo">Sexo</label>
                      <div class="col-md-4">
                      <div class="radio">
                        <label for="sexo-0">
                          <input type="radio" name="sexo" id="sexo-0" value="m"  {{old('sexo',$client->sexo) == 'm' ? 'checked="checked"' : ''}}>
                          Masculino
                        </label>
                        </div>
                      <div class="radio">
                        <label for="sexo-1">
                          <input type="radio" name="sexo" id="sexo-1" value="f"  {{old('sexo',$client->sexo) == 'f' ? 'checked="checked"' : ''}}>
                          Feminino
                        </label>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label" for="deficiencia_fisica">Deficiência física</label>
                      <div class="col-md-4">
                      <input id="deficiencia_fisica" name="deficiencia_fisica" value="{{old('deficiencia_fisica',$client->deficiencia_fisica)}}" type="text" placeholder="Deficiência física" class="form-control input-md">

                      </div>
                    </div>
                @endif

                <!-- Multiple Checkboxes -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="inadimplente">Inadimplente?</label>
                  <div class="col-md-4">
                  <div class="checkbox">
                    <label for="inadimplente-0">
                      <input type="checkbox" name="inadimplente" id="inadimplente-0" value="1"  {{old('inadimplente',$client->inadimplente) == '1' ? 'checked="checked"' : ''}}>
                      sim
                    </label>
                    </div>
                  </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for=""></label>
                  <div class="col-md-4">
                    <button id="" name="" class="btn btn-primary">Atualizar</button>
                  </div>
                </div>

                </fieldset>
            </form>


        </div>
    </div>


@endsection

