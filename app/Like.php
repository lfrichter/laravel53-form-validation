<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

    /** Já que não podemos definir o que é (Imagem ou Texto) por ser polimórfico, simplesmente falamos que é um model curtivel ou likeable */
    public function likeable()
    {
        return $this->morphTo();
    }
}
