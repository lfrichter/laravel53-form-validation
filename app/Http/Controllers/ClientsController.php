<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\ClientRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('clients.index', compact('clients')); //['clients' => 'Coleção de clients']
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $paramPessoa = $request->get('pessoa');
        $pessoa = Client::getPessoa($paramPessoa);

        return view('clients.create', compact('pessoa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        $client = new Client();
        $data = $request->all();
        $data['pessoa'] = Client::getPessoa($request->get('pessoa'));
        $data['inadimplente'] = $request->has('inadimplente') ? true : false;
        $client->create($data);
        return redirect()->route('clients.index');

//        $client->nome = $request->get('nome');
//        $client->documento = $request->get('documento');
//        $client->telefone = $request->get('telefone');
//        $client->email = $request->get('email');
//        $client->data_nasc = $request->get('data_nasc');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$client = Client::find($id)){
            throw new ModelNotFoundException("Cliente não foi encontrado");
        }
        $pessoa = $client->pessoa;
        return view('clients.edit', compact('client','pessoa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, $id)
    {
        if(!$client = Client::find($id)){
            throw new ModelNotFoundException("Cliente não foi encontrado");
        }
        $data = $request->all();
        $data['inadimplente'] = $request->has('inadimplente') ? true : false;
        $client->fill($data);
        $client->save();

        return redirect()->route('clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$client = Client::find($id)){
            throw new ModelNotFoundException("Cliente não foi encontrado");
        }
        $client->delete();
        return redirect()->route('clients.index');
    }
}
