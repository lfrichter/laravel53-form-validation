<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CoursesController extends Controller
{

     public function index(Request $request)
    {
        $str = $request->get('str', "");

        if($str){

            $courses = Course::search($str)->get();

//            $courses = Course::where('name', 'like', '%'.$str.'%')->
//                orWhere('description','like','%'.$str.'%')->get();
        }else{
            $courses = Course::all();
        }

        return view('courses.algolia', ['courses' => $courses, 'str' => $str]);
    }
}
