<?php

namespace App;

use App\Tag;
use App\Like;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['description', 'resolution'];

    public function likes()
    {
        /** prefixo identificador da relação polimórfica declarado na migration: $table->morphs('likeable'); */
        return $this->morphMany(Like::class, 'likeable');
    }

    public function tags(){
        /** prefixo identificador da relação polimórfica declarado na migration: $table->morphs('taggable'); */
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
