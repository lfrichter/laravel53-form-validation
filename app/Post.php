<?php

namespace App;

use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'body'];
    protected $dates = ['published_at', 'deleted_at'];
    protected $connection = 'sqlite';
    protected $primaryKey = 'post_id';
    protected $table = 'tb_posts';
    public $timestamps = false;
    protected $casts = [
        'published_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    /*protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('published', function(Builder $builder){
           $builder->where('published_at', '<', Carbon::now()->format('Y-m-d H:i:s'));
        });
    }*/

    public function scopeOfType($query, $type = 'text')
    {
        return $query->where('type', $type);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        //return $this->belongsToMany(Tag::class, 'posts_tag', 'post_id', 'tag_id');
        return $this->belongsToMany(Tag::class);
    }

    /** Mutator - seta title em em caixa baixa para ser registrado no banco */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = strtolower($value);
    }

    /** Acessor - Ajusta title que vem do banco, para primeira letra caixa alta */
    public function getTitleAttribute($value)
    {
        return ucfirst($value);
    }
}
