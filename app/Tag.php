<?php

namespace App;


use App\Text;
use App\Image;
use App\Post;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;


    public function posts()
    {
        return $this->belongsToMany(Tag::class);
    }

    /** Retorna a lista de tags para Text */
    public function texts()
    {
        return $this->morphedByMany(Text::class, 'taggable');
    }

    /** Retorna a lista de tags para Image */
    public function images()
    {
        return $this->morphedByMany(Image::class, 'taggable');
    }
}
