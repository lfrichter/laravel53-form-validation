<?php

use Illuminate\Database\Seeder;
use App\Course;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(ClientsTableSeeder::class);

        factory(Course::class, 1000)->create(); //Cria os registros no banco de dados

        // - make(); cria as instancias mas não grava no banco de dados

    }
}
