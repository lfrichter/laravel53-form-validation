<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Implementações

Laravel is a web application framework with expressive, elegant syntax. 

- [Iniciando com Laravel 5.3](https://www.schoolofnet.com/curso-iniciando-com-laravel-5-3/).
- [Laravel 5.3: Validações e Formulários](https://www.schoolofnet.com/curso-laravel-53-validacoes-e-formularios/).
- [Iniciando com Laravel](https://www.schoolofnet.com/curso-iniciando-com-laravel/).
- [Banco de dados e Eloquent](https://www.schoolofnet.com/curso-banco-de-dados-e-eloquent/).
- [Laravel 5.3: Scout e Realtime Search no Frontend](https://www.schoolofnet.com/curso-laravel-53-scout-e-realtime-search-no-frontend/).

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
