<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Client
 *
 * @property int $id
 * @property string $nome
 * @property string $documento
 * @property string $email
 * @property string $telefone
 * @property bool $inadimplente
 * @property string $data_nasc
 * @property string $sexo
 * @property string $estado_civil
 * @property string $deficiencia_fisica
 * @property string $fantasia
 * @property string $pessoa
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereDataNasc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereDeficienciaFisica($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereDocumento($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereEstadoCivil($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereFantasia($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereInadimplente($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereNome($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client wherePessoa($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereSexo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereTelefone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereUpdatedAt($value)
 */
	class Client extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

